#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- by Alex Berber -*-

import httplib
import requests

class BotUtils(object):

    # URLs
    api_url = "https://api.telegram.org/bot"
    bot_token = "213574497:AAF4UYBFeEl7nN7dviur7LUFT6M0QknB1Zk"

    # Methods
    send_message_method = '/sendMessage'
    full_url = api_url + bot_token

    def monitor_site(self):
        """monitor LinuxSpace site"""
        host = "https://www.linuxspace.org/"
        try:
            request_status = requests.get(host)
            site_status = request_status.status_code
            status_final = "Site LinuxSpace.org is UP! Http Code: " + str(site_status)
            requests.post(self.full_url + self.send_message_method, data={'chat_id': 718943, 'text': str(status_final)})
            return status_final
        except requests.ConnectionError:
            print("failed to connect")

    def get_photo_from_user(self):
        """user sends photo to Telegram bot
        with tag #israelpolice #parkaspig"""
        pass

    def send_photo_facebook_police(self):
        """photo posts on special Facebook account
        with tag police"""
        pass


