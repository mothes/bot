#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- by Alex Berber -*-

import json

import requests
from flask import Flask, request, Response
from BotUtils.BotUtils import BotUtils
import urllib

class BotEnginX(object):

    api_url = "https://api.telegram.org/bot"
    bot_token = "219166329:AAFYwf8mUtYdz7hHcXMWYes2mF-9ZifftVw"
    bot_status = "https://api.telegram.org/bot219166329:AAFYwf8mUtYdz7hHcXMWYes2mF-9ZifftVw/getMe"
    send_message_method = '/sendMessage'
    full_url = api_url + bot_token

    utilities = BotUtils()

    def get_me(self):
        bot_status = urllib.urlopen(self.bot_status).getcode()
        status_final = str(bot_status)
        return status_final

    def check_got_post(self):
        got_post = "I got POST method"
        return got_post

    def request_data(self):

        site_status = "/status"

        data = request.data
        json_raw = json.loads(data)
        print "=================================="
        print data
        print "=================================="

        got_msg = json_raw['message']['text']
        from_user_id = json_raw['message']['from']['id']
        first_name = json_raw['message']['from']['first_name']
        second_name = json_raw['message']['from']['last_name']
        full_name = first_name + " " + second_name
        nick_name = json_raw['message']['from']['username']
        chat_id = json_raw['message']['chat']['id']

        print "-- json data --\n%s" % data + "\n"
        print "-- user data --"
        print "Text Message: %s" % got_msg
        print "From User ID: %s" % from_user_id
        print "Chat ID: %s" % chat_id
        print "From User full name: %s" % full_name
        print "From Nickname of user: %s" % "@" + nick_name

        if from_user_id != '718943':
            values = {}
            values['text'] = "Hello " + full_name + " you are not authorized to send commands!"
            requests.post(self.full_url + self.send_message_method, data=values)

        # Check CMD from User
        if got_msg == site_status:
            self.utilities.monitor_site()

        resp = "Thank you!"
        return resp, 200, {'Content-Type': 'text/css; charset=utf-8'}



