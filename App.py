#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- by Alex Berber -*-

''' Set webhook for Telegram https://api.telegram.org/bot213574497:AAF4UYBFeEl7nN7dviur7LUFT6M0QknB1Zk/setWebhook?url=https://telegram.upods.io/route
'''

from flask import Flask, request
from Bot.BotEngineX import BotEnginX

app = Flask(__name__)
app.debug = True

@app.route('/')
def main_page():
    say_hello = "Telegram Bot StartPage!"
    return say_hello

@app.route('/route', methods=['GET', 'POST'])
def bot_entry_point():
    if request.method == "POST":
        get_data_obj = BotEnginX()
        return get_data_obj.request_data()

    if request.method == "GET":
        method = "I Got GET method"
        return method

if __name__ == '__main__':
    app.run()


